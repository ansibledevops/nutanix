Nutanix Cluster Role
=========

This role configures the following using the Nutanix API and ncli. ***ncli was used since there was no API available***

- Name Servers
- NTP Servers
- AD Authentication
  - Role Mappings
- VLANS
- Cluster Name
- Cluster Virtual IP
- Data Services IP
- Network Switch (Stat Gathering)
- Email Alerting
- Prism Central Registration

This role was designed to config mulitple clusters in different locations.

***Role has been tested on AOS v5.8.1***

Requirements
------------

- nCli is required to be installed on the controller machine.
- User setup for API access on the cluster


Role Variables
--------------

Variables prefixed with vault_ are passwords that should be stored in a vault.

#### Applicable to all


ansible_user: "{{vault_cvm_user}}". # Don't think I currently use since its all API and local ncli commands

Preferred method is to use the API whenever possible. Create an account through the Prism UI or CLI to use for calling it.

```yaml
ntnx_api_user: "{{vault_ntnx_api_user}}"
ntnx_api_pass: "{{vault_ntnx_api_pass}}"
```

General Cluster settings

```yaml
cluster_name: ntnx-cluster     # Name to assign to the cluster
cluster_virtual_ip: 10.0.0.1   # Cluster Virtual IP
data_services_ip: 10.0.0.2     # Cluster iSCSI Data Services IP

# List of ntp servers to use
ntp_servers:
  - 0.pool.ntp.org
  - 1.pool.ntp.org
  - 2.pool.ntp.org

# The servers to use for the cluster. Suggest using AD DNS if you are setting up AD Authentication
name_servers:
  - 8.8.8.8
  - 8.8.4.4

# List of emails you want alerts to be sent to
alert_emails:
  - email@domain.com

# AD Configuration settings, variables prefix with vault_ should be stored in a vault
ad_config:
  connection_type: LDAP
  directory_type: ACTIVE_DIRECTORY
  directory_url: ldap://your.ad.domain.com:389
  domain: ad.domain.com
  group_search_type: RECURSIVE
  name: ADConfig
  service_account_username: "{{vault_service_account_user}}"
  service_account_password: "{{vault_service_account_password}}"

# Set the AD groups you want to have access to the cluster and their respective permissions
role_mappings:
  - directoryName: DefaultRoleMappings
    entityType: GROUP
    entityValues:
      - NTX-Users
    role: ROLE_CLUSTER_ADMIN
  - directoryName: DefaultRoleMappings
    entityType: GROUP
    entityValues:
      - NTX-Admins
    role: ROLE_USER_ADMIN

# SNMP settings for creating the profile for network switch stat collection
switch_config_snmp_profile:
  name: Default Switch Profile
  version: snmpv2c
  community: "{{vault_snmp_community}}"

# IP of the Switches for network config stats
switches: 
  - 10.0.0.101
  - 10.0.0.102

# List of the VLAN's you want setup on the cluster
vlans:
  - name: Client VLAN
    vlan_id: 100
  - name: Server VLAN
    vlan_id: 101

# Path to the ncli install on the control machine (must include /)
ncli_path: ~/Downloads/ncli/

# Prism Central Details
prism_central_ip: ip_of_prism_central
prism_central_api_user: "{{vault_service_account_user}}"
prism_central_api_password: "{{vault_service_account_pass}}"

```

A description of the settable variables for this role should go here, including any variables that are in defaults/main.yml, vars/main.yml, and any variables that can/should be set via parameters to the role. Any variables that are read from other roles and/or the global scope (ie. hostvars, group vars, etc.) should be mentioned here as well.

Tags
----

- nameservers
- ntp
- auth
- roles
- vlan
- cluster
- switch
- alerts
- prism_central

```
ansible-playbook nutanix.yml --tags "any of the tags listed above, comma seperated" 
```

Dependencies
------------

None

Example Playbook
----------------

```yaml
- hosts: servers
  gather_facts: no  # If only using this playbook, no point in gathering facts since we are using API and the localhost for commands
  tasks:
  - import_role:
      name: nutanix
```


License
-------

BSD

Author Information
------------------

Martin Fedec